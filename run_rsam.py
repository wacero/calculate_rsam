
import os,sys
import logging, logging.config
from obspy.core import UTCDateTime
from datetime import timedelta, datetime
import pandas as pd 

from get_mseed_data import get_mseed
from get_mseed_data import get_mseed_utils as gmutils
from rsam import rsam_utils
from rsam import db_connection
from rsam import rsam

if gmutils.check_file("./config/logging.ini"):
    
    logging.config.fileConfig('./config/logging.ini', disable_existing_loggers=False)
    logger=logging.getLogger('run_rsam')

pd.set_option('display.max_rows',None)
pd.set_option('display.max_columns',None)
pd.set_option('expand_frame_repr', False)


def main():
    """
    ##COMO CORREGIR PROBLEMAS CON ESTACIONES CON GAPS/OVERLAPS? e.g. BRRN
    This script parse the configuration text file, set the parameters and runs the rsam module
    """
   
    is_error=False
    
    if len(sys.argv)==1:
        is_error=True
    
    else:   
        try:
            run_param=gmutils.read_parameters(sys.argv[1])
        
        except Exception as e:
            logger.error(f"Error reading configuration sets in file: {e}")
            raise Exception(f"Error reading configuration file: {e}")
        try:
            run_mode = run_param['RUN_MODE']['mode'] #Available options are REAL_TIME|FILL
            data_target = run_param['RUN_MODE']['data_target'] #Available options are STDOUT|CSV|DB MSSQL, INFLUX
            db_id=run_param['DATABASE']['id']
            db_config_file= run_param['DATABASE']['file_path']
            db_type = run_param['DATABASE']['type'] #available options are MSSQL|INFLUX
            db_mode=run_param['DATABASE']['mode'] #Available options are INSERT| UPDATE
            mseed_id=run_param['MSEED_SERVER']['id']
            mseed_server_config_file=run_param['MSEED_SERVER']['file_path']
            station_file=run_param['STATION_FILE']['file_path']
            filter_file = run_param['FILTER_FILE']['file_path']
            
        except Exception as e:
            logger.error(f"Error getting parameters: {e}")
            raise Exception(f"Error getting parameters: {e}")
        
        try:
            db_param=gmutils.read_config_file(db_config_file)
            mseed_server_param=gmutils.read_config_file(mseed_server_config_file)
            stations_dict=gmutils.read_config_file(station_file)
        except Exception as e:
            logger.error(f"Error reading configuration file: {e}")
            raise Exception(f"Error reading configuration file: {e}")

        
        try:
            mseed_client=get_mseed.choose_service(mseed_server_param[mseed_id])
            
        except Exception as e:
            logger.error("Failed to create mseed client: %s" %str(e))
            raise Exception("Failed to create mseed client: %s" %str(e))
        
        """Create filter list"""
        try:
            filter_dict = gmutils.read_config_file(filter_file)
            filter_list = rsam.create_rsamfilter_list(filter_dict)
        except Exception as e:
            logger.error("Failed to read filters file %s" %str(e))
            raise Exception("Failed to read filters file %s" %str(e))
          
        """Create streams_list according to run_mode"""
        
        if run_mode == "REAL_TIME":
            try:
                day_utc=UTCDateTime(UTCDateTime.now().strftime("%Y-%m-%d %H:%M:00")) - 300
                streams_list = rsam.create_stream_list(mseed_id,mseed_client,stations_dict,day_utc,60,60,run_mode)     
            except Exception as e:
                raise Exception("Error getting list of streams: %s" %str(e))
            
        elif run_mode == "FILL":
        
            if len(sys.argv) == 4:
                
                start_date = UTCDateTime(datetime.strptime(sys.argv[2],"%Y-%m-%d"))
                end_date = UTCDateTime(datetime.strptime(sys.argv[3],"%Y-%m-%d"))
                
                streams_list=[]
                for day in rsam_utils.perdelta(start_date,end_date,timedelta(days=1)):
                
                    day_utc = UTCDateTime(day)
                    #streams_list = rsam.create_stream_list(mseed_id,mseed_client,stations_dict,day_utc,86400,60,run_mode)
                    streams_list.extend(rsam.create_stream_list(mseed_id,mseed_client,stations_dict,day_utc,86400,60,run_mode))
            else:
                usage_message = (f'USAGE: python {sys.argv[0]} CONFIGURATION_FILE.txt ' 'start_date end_date')
                error_message = (f"Error in run_mode configured: {run_mode}. " "start_date and end_date needed")
                
                print(usage_message)
                logger.error(error_message)
                raise Exception(error_message)



        else:
            logger.error("Error in run_mode configured: %s" %run_mode)
            raise Exception("Error in run_mode configured: %s" %run_mode)
        
        """
        Create the rsam_list using streams_list
        """
        try:
            rsam_list = []
            for stream in streams_list[:]:
                
                rsam_list.append(rsam.calculate_rsam(stream,filter_list))
        except Exception as e:
            logger.error("Failed to create streams_list: %s" %str(e))
            raise Exception("Failed to create streams_list: %s" %str(e))
        
        """
        Manage data acording to data_target
        """
        if data_target == "CSV":
            rsam_dataframe = pd.DataFrame.from_records(rsam_list)
            rsam_dataframe.to_csv('./rsam_%s_%s.csv' %(start_date,end_date), sep=',', index= False)
 
        elif data_target == "STDOUT":
            
            rsam_dataframe = pd.DataFrame.from_records(rsam_list)
            #print(rsam_dataframe)
            
        elif data_target == "DB" and db_type == "MSSQL":
            
            try:

                db_client=db_connection.createConexionDB(db_param[db_id]['host'],db_param[db_id]['port'],
                                                         db_param[db_id]['user'],db_param[db_id]['pass'],db_param[db_id]['DBName'])

            except Exception as e:
                logger.error("Failed to create database client: %s" %str(e) )
                raise Exception("Failed to create database client: %s" %str(e))
            
            if db_mode == "INSERT":
                try:
                    for val in rsam_list: 
                        row=(val['rsam_station'],val['rsam_channel'],val['rsam_datetime'].strftime("%Y-%m-%dT%H:%M:%S"),val['rms'],
                             val['rsam_banda1'],val['rsam_banda2'],val['rsam_banda3'],val['rsam_banda4'],val['rsam_banda5'])
                        
                        db_connection.insert_row(db_client,row)
                    
                except Exception as e:
                    logger.error("Failed in insert to database MSSQL: %s" %str(e))
                    raise Exception("Failed in insert to database MSSQL: %s" %str(e))
            
            elif db_mode == "UPDATE":
            
                try:
                    for val in rsam_list: 
                        row=(val['rsam_station'],val['rsam_channel'],val['rsam_datetime'].strftime("%Y-%m-%dT%H:%M:%S"),val['rms'],
                             val['rsam_banda1'],val['rsam_banda2'],val['rsam_banda3'],val['rsam_banda4'],val['rsam_banda5'])
                        db_connection.update_row(db_client,row)
                    
                except Exception as e:
                    logger.error("Failed in update to database MSSQL: %s" %str(e))
                    raise Exception("Failed in update to database MSSQL: %s" %str(e))
        
        elif data_target ==  "DB" and db_type == "INFLUX":    
            logger.info("Insert into INFLUX")
            influxdb_client = db_connection.get_influxdb_client(db_param[db_id]['host'],db_param[db_id]['port'],db_param[db_id]['user'],
                                                         db_param[db_id]['pass'],db_param[db_id]['DBName'])
            
            db_connection.insert_rsam(influxdb_client, db_param[db_id]['DBName'],rsam_list,filter_list)
            
        else:
            logger.error("Error in data_target configured: %s" %data_target)
            raise Exception("Error in data_target configured: %s" %data_target)
    
    if is_error:
        logger.info(f'Usage: python {sys.argv[0]} configuration_file.txt [start_date] [end_date]')
        print(f'USAGE: python {sys.argv[0]} CONFIGURATION_FILE.txt [start_date] [end_date]')    


if __name__ == "__main__":

    main()


