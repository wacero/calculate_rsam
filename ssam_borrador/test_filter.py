import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import butter, filtfilt
from obspy import read
from obspy.core import UTCDateTime
import sys

# Cargar la traza
data_path_1 = "/datosSC3/2024/EC/SAG1/BDF.D/EC.SAG1.05.BDF.D.2024.178"
st = read(data_path_1)

st.plot()

st.filter("highpass",freq=0.5)

st.plot()


sys.exit()
trace = st[0]

# Definir el periodo de interés
start_time = UTCDateTime("2023-02-23 00:20:00")
end_time = UTCDateTime("2023-02-23 03:10:00")
trace.trim(starttime=start_time, endtime=end_time)

# Preprocesamiento: eliminar la tendencia y detrend
trace.detrend("demean")

# Obtener la FFT de la señal original
fft_original = np.fft.fft(trace.data)
frequencies = np.fft.fftfreq(len(trace.data), d=1/trace.stats.sampling_rate)

# Configurar el filtro
lowcut = 0.05  # Frecuencia de corte de 0.1 Hz
b, a = butter(4, lowcut / (0.5 * trace.stats.sampling_rate), btype='low')
filtered_data = filtfilt(b, a, trace.data)

# Obtener la FFT de la señal filtrada
fft_filtered = np.fft.fft(filtered_data)

# Graficar los espectros
plt.figure(figsize=(14, 6))

# Espectro original
plt.subplot(1, 2, 1)
plt.plot(frequencies, np.abs(fft_original), label='Original', color='blue')
plt.title('Espectro de Frecuencia Original')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Amplitud')
plt.grid(True)
plt.xlim(0, 10)  # Limitar la visualización a 10 Hz
plt.legend()

# Espectro filtrado
plt.subplot(1, 2, 2)
plt.plot(frequencies, np.abs(fft_filtered), label='Filtrado', color='red')
plt.title('Espectro de Frecuencia Filtrado')
plt.xlabel('Frecuencia (Hz)')
plt.ylabel('Amplitud')
plt.grid(True)
plt.xlim(0, 10)  # Limitar la visualización a 10 Hz
plt.legend()

plt.tight_layout()
plt.show()
