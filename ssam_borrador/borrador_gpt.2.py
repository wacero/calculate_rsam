import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import spectrogram
import obspy
import csv
from obspy import UTCDateTime
from obspy import signal
import sys

##ESTE ES EL CODIGO QUE SIRVE !!!
# Suponiendo que estas variables están definidas
#window_length = 180  # Longitud de la ventana en segundos, original 60 segundos, muchos datos
#noverlap = 90  # Solapamiento en segundos

window_length = 60
noverlap = 30


# Leer el archivo MiniSEED

#data_path='/datosSC3/2022/EC/FER1/BHZ.D/EC.FER1..BHZ.D.2022.006'
#data_path="/datosSC3/2024/EC/FER1/BHZ.D/EC.FER1..BHZ.D.2024.132"
#data_path = "/datosSC3/2023/EC/CHL1/HHZ.D/EC.CHL1..HHZ.D.2023.068"
#data_path = "/datosSC3/2024/EC/CHL1/HHZ.D/EC.CHL1..HHZ.D.2024.127"

#data_path = "/datosSC3/2024/EC/REVS/HHZ.D/EC.REVS..HHZ.D.2024.107"

data_path = "/datosSC3/2024/EC/BNAS/BDF.D/EC.BNAS..BDF.D.2024.213"



st = obspy.read(data_path)

start_time = UTCDateTime("2024-07-31 16:30:00")
end_time = UTCDateTime("2024-07-31 23:30:00")
#start_time = UTCDateTime("2011-11-28 00:00:00")
#end_time = UTCDateTime("2011-11-28 23:59:00")


max_frequency = 100
decimation_factor = 10  # Ajusta este valor según sea necesario
trace_original = st[0]


trace_trimmed = trace_original.copy()
trace_trimmed.trim(starttime=start_time, endtime=end_time)
start_time = trace_trimmed.stats.starttime

trace_decimated = trace_trimmed.copy()

# Asegúrate de trabajar con una sola traza (Trace) para simplificar
#tr = st[0]



trace_decimated.decimate(decimation_factor, no_filter=True)

# Preprocesamiento: Detrend y filtrado si es necesario
trace_trimmed.detrend("demean")
#trace_trimmed.filter("bandpass", freqmin=0.1, freqmax=50)
#trace_trimmed.filter("lowpass", freq = 10, corners=4)

#trace_trimmed.data = signal.filter.lowpass(trace_trimmed.data,freq=1.0, df=trace_trimmed.stats.sampling_rate)




"""
tr = trace_trimmed.copy()

from scipy.signal import butter, filtfilt, freqz
import matplotlib.pyplot as plt
import numpy as np

# Configuración del filtro
b, a = butter(4, 0.1 / (tr.stats.sampling_rate / 2), 'low')  # Filtro Butterworth de orden 4

# Aplicar el filtro
filtered_data = filtfilt(b, a, tr.data)



print(len(filtered_data))
#tr.data = filtered_data
"""

tr = trace_trimmed.copy()


# Cálculo del número de puntos por segmento y del número de puntos de solapamiento

#nperseg = int(window_length * tr.stats.sampling_rate)
nperseg = int(tr.stats.sampling_rate/0.5)
noverlap = int(noverlap * tr.stats.sampling_rate)
station_name = tr.stats['station']

# Cálculo del SSAM
f, t, Sxx = spectrogram(tr.data, fs=tr.stats.sampling_rate,nperseg=nperseg, noverlap=nperseg//2)

print(len(f))
print(f)

# Limita la frecuencia a 300 Hz para la visualización
idx = f <= max_frequency

#"""

with open("espectrograma.csv", "w", newline="") as csvfile:
    writer = csv.writer(csvfile)
    #writer.writerow(["time_utc","timestamp", "tiempo_seg", "frecuencia", "amplitud", "amplitud_db","station"])
    writer.writerow(["time_utc","frecuencia", "amplitud_db","station"])

    for j, time in enumerate(t):
        for i, freq in enumerate(f[idx]):
            amplitude = Sxx[i, j]

            temp_time = start_time + time

            amplitude_db = 10*np.log10(Sxx[i,j])

            if amplitude_db > 30 and freq <= 15:

                ##writer.writerow([ temp_time, temp_time.timestamp, time, freq, amplitude, amplitude_db,station_name])
                writer.writerow([ temp_time, freq, amplitude_db,station_name])
#"""


#'''
# Genera el gráfico SSAM

cmap = plt.get_cmap('jet')
plt.set_cmap(cmap)

plt.figure(figsize=(12, 6))
plt.pcolormesh(t, f[idx], 10 * np.log10(Sxx[idx, :]), shading='gouraud')
plt.colorbar(label='Intensidad [dB]')
plt.ylabel('Frecuencia [Hz]')
plt.xlabel('Tiempo [s]')
plt.title('Gráfica SSAM')
plt.ylim(0, max_frequency)  # Limita el eje y a 300 Hz
plt.show()
#'''