import sys
import csv

sys.path.append("/home/wacero/proyectos_codigo/calculate_rsam/rsam")


print(sys.path)

from rsam import db_connection


try:
    influxdb_client = db_connection.get_influxdb_client("192.168.1.90", "8086" , "sangay","sangay2020" ,"ssam")


    print(influxdb_client)

except Exception as e:
    print(f"Error creating influx client: {e}")        



#db_connection.insert_rsam(influxdb_client, db_param[db_id]['DBName'],rsam_list,filter_list)


with open("espectrograma.csv") as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        json_body = [
            {
                "measurement": "espectrograma",
                "time": row['time_utc'],
                "tags": {
                    "frecuencia": row['frecuencia']
                },
                "fields": {
                    "amplitud": float(row['amplitud']),
                    "amplitud_db": float(row['amplitud_db'])
                }
            }
        ]
        influxdb_client.write_points(json_body)

influxdb_client.close()