import numpy as np
import matplotlib.pyplot as plt
import obspy
from scipy.fft import fft

def nextpow2(N):
    """ Function for finding the next power of 2 """
    n = 1
    while n < N: n *= 2
    return n



#data_path='/datosSC3/2022/EC/FER1/BHZ.D/EC.FER1..BHZ.D.2022.006'
data_path = "/datosSC3/2023/EC/CHL1/HHZ.D/EC.CHL1..HHZ.D.2023.054"
# Paso 1: Leer los datos sísmicos
st = obspy.read(data_path)

# Asume que st es tu Stream que contiene una hora de datos
# Paso 2: Preprocesamiento (detrend, demean, filtering)
st.detrend(type='demean')
#st.filter("bandpass", freqmin=0.1, freqmax=20)

# Paso 3 y 4: Segmentar y calcular el espectro para cada ventana de tiempo
window_length = 60  # ventana de tiempo en segundos
noverlap = 30  # solapamiento en segundos
nfft = 2 ** nextpow2(window_length * st[0].stats.sampling_rate)  # Longitud del FFT

nperseg = int(window_length * st[0].stats.sampling_rate)
noverlap = int(noverlap * st[0].stats.sampling_rate)


# Define tus bandas de frecuencia aquí
freq_bands = [(0.1, 1), (1, 2), (2, 4), (4, 8), (8, 16)]

# Paso 5 y 6: Visualización
fig, ax = plt.subplots(figsize=(10, 5))

for window_start in range(0, int(len(st[0].data)), window_length * st[0].stats.sampling_rate - noverlap):
    window_end = window_start + window_length * st[0].stats.sampling_rate
    window_data = st[0].data[window_start:window_end]
    
    # FFT
    spectrum = fft(window_data, nfft)
    freqs = fftfreq(nfft, 1 / st[0].stats.sampling_rate)
    
    # Amplitud para cada banda de frecuencia
    for band in freq_bands:
        band_freqs = (freqs >= band[0]) & (freqs <= band[1])
        # Puedes hacer un promedio de la amplitud o sumarla
        amplitude = np.mean(np.abs(spectrum[band_freqs]))
        
        # Dibuja un pixel o una barra para la amplitud en esta banda y ventana de tiempo
        ax.plot(window_start / st[0].stats.sampling_rate, amplitude, ...)

# Configura los límites del eje y el título
ax.set_xlim(0, 3600)  # Una hora en segundos
ax.set_ylim(0, ... ) 
