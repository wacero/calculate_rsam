import obspy
import numpy as np
import matplotlib.pyplot as plt



def calculate_ssam(waveform_data, frequency_bands, num_intervals, samples_per_interval):

    # Inicializar una matriz para almacenar las amplitudes espectrales promedio
    ssam = np.zeros((len(frequency_bands) - 1, num_intervals))

    # Iterar sobre cada intervalo
    for i in range(num_intervals):
        # Obtener datos de la forma de onda para este intervalo
        start_sample = i * samples_per_interval
        end_sample = (i + 1) * samples_per_interval
        interval_data = waveform_data[start_sample:end_sample]

        # Calcular la FFT
        fft = np.fft.fft(interval_data)
        fft_amplitude = np.abs(fft[:len(fft)//2])  # Solo se consideran las frecuencias positivas

        # Calcular las frecuencias correspondientes
        freqs = np.fft.fftfreq(len(interval_data), 1/sampling_rate)[:len(fft)//2]

        # Calcular la amplitud espectral promedio para cada banda de frecuencia
        for j in range(len(frequency_bands) - 1):
            lower_bound = frequency_bands[j]
            upper_bound = frequency_bands[j + 1]
            in_band = (freqs >= lower_bound) & (freqs < upper_bound)
            ssam[j, i] = np.mean(fft_amplitude[in_band])

    return ssam





if __name__ == "__main__":
    # Your main script execution code here



    file_path = '/datosSC3/2023/EC/CHL1/HHZ.D/EC.CHL1..HHZ.D.2023.054'


    # Generar bandas de frecuencia logarítmicas
    min_freq = 1  # Frecuencia mínima
    max_freq = 100 / 2  # Frecuencia máxima (Nyquist)
    num_bands = 6  # Número de bandas de frecuencia
    frequency_bands = np.geomspace(min_freq, max_freq, num_bands)
    interval = 5  # 1 segundo

    inicio = obspy.UTCDateTime("2023-02-23 06:22:00")

    # Leer el archivo MiniSEED
    st = obspy.read(file_path)

    tr_temp = st[0]  # Asumiendo un solo trazo en el archivo
    st_temp = st.trim(inicio, inicio+1200 )
    tr = st_temp[0]

    # Obtener datos de la forma de onda y la tasa de muestreo
    waveform_data = tr.data
    sampling_rate = tr.stats.sampling_rate

    # Calcular el número de muestras en cada intervalo
    samples_per_interval = int(sampling_rate * interval)

    # Calcular el número de intervalos en la forma de onda
    num_intervals = len(waveform_data) // samples_per_interval

    print("####")
    print(num_intervals)




    ssam_matrix = calculate_ssam(waveform_data, frequency_bands, num_intervals)

    # Visualizar SSAM
    plt.imshow(ssam_matrix, aspect='auto', cmap='jet', extent=[0, num_intervals, np.log10(min_freq), np.log10(max_freq)], origin='lower')
    plt.colorbar(label='Amplitude')
    plt.ylabel('Frequency (log scale)')
    plt.xlabel('Time Interval')
    plt.yticks(np.log10(frequency_bands), [f'{freq:.1f} Hz' for freq in frequency_bands])
    plt.show()


    '''

    # Definir las bandas de frecuencia y el intervalo de tiempo
    ##frequency_bands = [0, 1, 2, 4, 8, 16, 32]
    frequency_bands = [0.01, 0.1, 1, 10]
    interval = 1.0  # 1 segundo





    # Calcular SSAM
    file_path = '/datosSC3/2023/EC/CHL1/HHZ.D/EC.CHL1..HHZ.D.2023.054'

    ssam_matrix = calculate_ssam(file_path, frequency_bands, interval)

    # Visualizar SSAM
    plt.imshow(ssam_matrix, aspect='auto', cmap='jet')
    plt.colorbar(label='Amplitude')
    plt.ylabel('Frequency Band')
    plt.xlabel('Time Interval')
    plt.yticks(range(len(frequency_bands) - 1), [f'{frequency_bands[i]}-{frequency_bands[i+1]} Hz' for i in range(len(frequency_bands) - 1)])
    plt.show()

    '''