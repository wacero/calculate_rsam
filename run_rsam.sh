#!/bin/bash
exec 1> >(logger -s -t $(basename $0)) 2>&1

eval "$(/$HOME/anaconda3/bin/conda shell.bash hook)"

if [ $? != 0 ]
    then
        echo "Error ejecutar RSAM: error carga conda: CRONTAB_SCRIPT_ERROR">&2
fi

conda activate calculate_rsam

if [ $? != 0 ]
    then
        echo "Error ejecutar RSAM: error activar entorno CRONTAB_SCRIPT_ERROR">&2
fi
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd $DIR
python ./run_rsam.py ./production_configuration.txt

if [ $? != 0 ]
    then
        echo "Error ejecutar RSAM: error ejecutar run_rsam.py CRONTAB_SCRIPT_ERROR">&2
fi
