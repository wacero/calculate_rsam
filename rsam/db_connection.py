
#import pypyodbc
import pyodbc as pypyodbc
import json
import logging
from influxdb import InfluxDBClient
from influxdb import DataFrameClient


def createConexionDB(DBHost,port,user,pwd,DBName):
    """
    Establish a database connection using pypyodbc.

    This function attempts to create a connection to a database using the FreeTDS driver. 
    The connection parameters such as database host, port, user credentials, and database name
    must be provided.

    :param str DBHost: The hostname or IP address of the database server.
    :param str port: The port number on which the database server is listening.
    :param str user: The username used to authenticate with the database.
    :param str pwd: The password used to authenticate with the database.
    :param str DBName: The name of the database to connect to.
    :return: A connection object to the specified database.
    :rtype: pypyodbc.Connection
    :raises Exception: If the connection attempt fails, an exception is raised 
                       with a descriptive error message.

    Example usage:
    >>> conexion = createConexionDB('localhost', '1433', 'username', 'password', 'myDatabase')
    """    
    try:
        conexion=pypyodbc.connect("Driver=FreeTDS;SERVER=%s;port=%s;UID=%s;PWD=%s;DATABASE=%s" %(DBHost,port,user,pwd,DBName))
        return conexion
    except Exception as e:
        logging.error("Error in createConexionDB: %s" %str(e))
        raise Exception("Error in createConexionDB: %s" %str(e))
    
    
def get_influxdb_client(db_host, db_port,username,password,db_name):

    """
    Create a client connection to an InfluxDB database.

    This function initializes a connection to an InfluxDB database using the specified 
    connection parameters. It employs the InfluxDBClient for creating this connection.

    :param str db_host: The hostname or IP address of the InfluxDB server.
    :param int db_port: The port number on which the InfluxDB server is listening.
    :param str username: The username used to authenticate with the InfluxDB.
    :param str password: The password used to authenticate with the InfluxDB.
    :param str db_name: The name of the database to connect to in InfluxDB.
    :return: A client object representing the connection to the InfluxDB.
    :rtype: InfluxDBClient
    :raises Exception: If the connection attempt fails, an exception is raised 
                       with a descriptive error message.

    Example usage:
    >>> client = get_influxdb_client('localhost', 8086, 'user', 'password', 'myDatabase')
    """




    try:
        client = InfluxDBClient(host=db_host,port=db_port,username=username,password=password, database=db_name)
        return client
    except Exception as e:
        logging.error("Error in get_influxdb_client: %s" %str(e))
        raise Exception("Error in get_influxdb_client: %s" %str(e))
       
def closeConexion(conexion):
    conexion.close()
    
def queryStationID(conexion,statName):
    try:
        logging.info("Query station ID of : %s" %statName)
        query=conexion.cursor()
        query.execute('''SELECT esta_id FROM Estacion WHERE esta_code=?;''',(statName,))
        row=query.fetchone()
        if row:
            return(row[0])
        else:
            logging.info("QueryStationID: No station %s" %(str(statName)))
            return -1
    except Exception as e:
        logging.error("Failed queryStationID : %s. %s" %(statName, str(e)))
        return -1
        


def insert_row(db_client,row):
    
    station_id = queryStationID(db_client, row[0])
    if station_id != -1:
        try:
            query=db_client.cursor()
            query.execute("""INSERT INTO RSAM (esta_id_estacion,rsam_canal,rsam_fecha_utc,rsam_rms,rsam_banda1,rsam_banda2,rsam_banda3,rsam_banda4,rsam_banda5)
                             VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')"""
                          %(station_id,row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8]))
            query.commit()
        except Exception as e:
            logging.error("Error in update_row(): %s" %(str(e)))
            
    else:
        logging.info("No station %s,  no update_row()" %(row['rsam_estacion']))
    


def update_row(db_client,row):

    station_id = queryStationID(db_client, row[0])
    if station_id != -1:
        try:
            query=db_client.cursor()
            query.execute("""UPDATE RSAM SET rsam_rms='%s', rsam_banda1='%s', rsam_banda2='%s', rsam_banda3='%s', rsam_banda4='%s',
                             rsam_banda5='%s' WHERE  esta_id_estacion='%s' and rsam_canal='%s' and rsam_fecha_utc='%s'""" 
                          %(row[3],row[4],row[5],row[6],row[7],row[8],station_id,row[1],row[2]))
        except Exception as e:
            logging.error("Error in update_row(): %s" %(str(e)))
            
    else:
        logging.info("No station %s,  no update_row()" %(row['rsam_estacion']))


def insert_rsam(client_influxdb,influxdb_name,rsam_list,filter_list):
    
    data_list =[]
    #print(val['rsam_station'],val['rsam_channel'],val['rsam_datetime'].strftime("%Y-%m-%dT%H:%M:%S"),val['rms'],val['rsam_banda1'],
    #                  val['rsam_banda2'],val['rsam_banda3'],val['rsam_banda4'],val['rsam_banda5'])
        
    for val in rsam_list:
        for filter in filter_list:
        
            insert_query = "%s,station=%s,channel=%s,banda=%s rsam_value=%s %s" \
                            %(influxdb_name,val['rsam_station'],val['rsam_channel'],
                              filter.name,val[filter.name], int(val['rsam_datetime'].timestamp)) 
    
            data_list.append(insert_query)
    
    #print(data_list)
    #client_influxdb.write("%s,station=%s,channel=%s,banda=%s rsam_value=%s %s" 
    #                      %(influxdb_name,),{'db':influxdb_name})

    client_influxdb.write_points(data_list,time_precision="s",database=influxdb_name,protocol="line")

def get_influx_DF_client(host,port,user,passw,db_name):
    """  
    Obtiene un cliente influx
    
    :param string host: ip del servidor
    :param string port: puerto del servidor
    :param string user: usuario 
    :param string passw: contrasena
    :param string db_name: nombre de la base de datos 
    :return InfluxDBClient
    :Raises Exception e: error al crear el cliente influx
    """
    try:
        return DataFrameClient(host=host,port=port, username=user,password=passw,database=db_name)

    except Exception as e:
        raise Exception("Error creating influxdb DataFrame client: %s" %str(e))



def insert_ssam(ssam_df, client_ifxdb):

    """
    """
    #station_mag_pd.set_index('creation_time',inplace=True)
    measurement = 'spectrogram'
    #tags = ['station_id','station_magnitude_type','author','event_id']
    tags = ["frequency","station"]
    fields = ["amplitud_db"]
    try:
        logging.info("start of insert into influx")
        client_ifxdb.write_points(dataframe=ssam_df, measurement=measurement,
            tag_columns=tags, field_columns=fields, protocol='line')

    except Exception as e:
        logging.error("Error in insert_ssam(): %s" %e)
