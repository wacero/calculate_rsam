rsam modules
=============

The modules from this package recover waveform data of a station, calculate the rms and store it in a DB

Module rsam
------------
.. automodule:: rsam.rsam
    :members:

Module rsam_utils
-----------------
.. automodule:: rsam.rsam_utils
    :members:

Module db_connection
-------------------- 
.. automodule:: rsam.db_connection
    :members:

.. mermaid::

   graph TD
       A[Christmas] -->|Get money| B(Go shopping)
       B --> C{Let me think}
       C -->|One| D[Laptop]
       C -->|Two| E[iPhone]
       C -->|Three| F[Car]

