.. calculate_rsam documentation master file, created by
   sphinx-quickstart on Mon Sep  2 07:51:26 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Calculate_rsam's documentation
==========================================

Library to calculate the RSAM (Real-time Seismic Amplitude Measurement) of seismic 
waveforms to use as input for an volcanic alert system

Description
-----------
The code retrieves waveform data of a station, calculates the rms and 
stores it in a MSSQL DB to be plotted by SAM.  


Requirements
-------------
The following packages must be installed 

.. code-block:: bash

    $ conda install obspy 
    $ conda install pyodbc 
    $ conda install json 


Execution
---------
To execute the module run the following command

.. code-block:: bash

    $ cd calculate_rsam
    $ python ./run_rsam.py CONFIGURATION_FILE.TXT

To view the logs, run the following command

.. code-block:: bash

    $ cd calculate_rsam
    $ tail -f ./rsam.log

Testing
-------
The test can be executed with the following command

.. code-block:: bash

    $ cd calculate_rsam 
    $ python -m pytest -v test/test_rsam.py

Rsam execution Flow 
-------------------
.. mermaid::

    graph TD
        A[Start] --> B{Check Command Line Arguments}
        B -- Missing/Invalid --> X[Log Error and Exit]
        B -- Valid --> C[Read Configuration Files]
        C --> D{Determine Run Mode}
        D -- REAL_TIME --> E[Process Real-Time Data]
        D -- FILL --> F[Process Historical Data]
        E --> G[Calculate RSAM]
        F --> G
        G --> H{Determine Data Target}
        H -- CSV --> I[Write to CSV]
        H -- STDOUT --> J[Print to STDOUT]
        H -- DB MSSQL --> K[Insert into MSSQL Database]
        H -- DB INFLUX --> L[Insert into INFLUX Database]
        I --> M[End]
        J --> M
        K --> M
        L --> M
        X --> M

Rsam sequence diagram
---------------------

.. mermaid::

    sequenceDiagram
        participant User
        participant Script as run_rsam.py
        participant Config as Configuration Module
        participant RSAM as RSAM Calculation Module
        participant DB as Database Module

        User->>+Script: Execute with Parameters
        Script->>+Config: Read Configuration
        Config-->>-Script: Return Parameters
        Script->>+RSAM: Process Data
        RSAM-->>-Script: Return Processed Data
        Script->>+DB: Store Data
        alt CSV Storage
            DB->>Script: Write to CSV
        else STDOUT
            DB->>Script: Print to STDOUT
        else MSSQL Database
            DB->>Script: Insert to MSSQL
        else INFLUX Database
            DB->>Script: Insert to INFLUX
        end
        Script-->>-User: Execution Complete




.. toctree::
   :maxdepth: 2
   :caption: Contents:

   rsam

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
