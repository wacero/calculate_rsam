#!/bin/bash
exec 1> >(logger -s -t $(basename $0)) 2>&1

eval "$(/$HOME/anaconda3/bin/conda shell.bash hook)"

if [ $? != 0 ]
    then
        echo "Error ejecutar SSAM: error carga conda: CRONTAB_SCRIPT_ERROR">&2
fi

conda activate calculate_rsam

if [ $? != 0 ]
    then
        echo "Error ejecutar SSAM: error activar entorno CRONTAB_SCRIPT_ERROR">&2
fi
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd $DIR
python ./run_ssam.py ./chiles_influx_ssam.txt

if [ $? != 0 ]
    then
        echo "Error ejecutar SSAM: error ejecutar run_ssam.py CRONTAB_SCRIPT_ERROR">&2
fi
