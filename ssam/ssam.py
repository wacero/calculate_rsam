import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import spectrogram
import obspy
import pandas as pd
from obspy import UTCDateTime
from obspy import signal
import sys

##ESTE ES EL CODIGO QUE SIRVE !!!
# Suponiendo que estas variables están definidas
#window_length = 180  # Longitud de la ventana en segundos, original 60 segundos, muchos datos
#noverlap = 90  # Solapamiento en segundos

window_length = 60
noverlap = 30
decimation_factor = 10  # Ajusta este valor según sea necesario
MIN_AMPLITUDE = 30
MAX_FREQUENCY = 15

# Leer el archivo MiniSEED

data_path="/datosSC3/2024/EC/FER1/BHZ.D/EC.FER1..BHZ.D.2024.132"
st = obspy.read(data_path)
trace_original = st[0]
trace_trimmed = trace_original.copy()
start_time = trace_trimmed.stats.starttime
trace_decimated = trace_trimmed.copy()
trace_decimated.decimate(decimation_factor, no_filter=True)

# Preprocesamiento: Detrend y filtrado si es necesario
trace_trimmed.detrend("demean")
#trace_trimmed.filter("bandpass", freqmin=0.1, freqmax=50)
#trace_trimmed.filter("lowpass", freq = 10, corners=4)
#trace_trimmed.data = signal.filter.lowpass(trace_trimmed.data,freq=1.0, df=trace_trimmed.stats.sampling_rate)

tr = trace_trimmed.copy()


# Cálculo del número de puntos por segmento y del número de puntos de solapamiento

def calculate_ssam(tr, MIN_AMPLITUDE, MAX_FREQUENCY, noverlap,window_length, frequency_bin):

    print(tr.stats)
    ##nperseg = int(window_length * tr.stats.sampling_rate)
    nperseg = int(tr.stats.sampling_rate/frequency_bin)

    
    noverlap = int(noverlap * tr.stats.sampling_rate)
    station_name = tr.stats['station']
    start_time = tr.stats.starttime

    # Cálculo del SSAM
    ##f, t, Sxx = spectrogram(tr.data, fs=tr.stats.sampling_rate, nperseg=nperseg, noverlap=noverlap)

    f, t, Sxx = spectrogram(tr.data, fs=tr.stats.sampling_rate, nperseg=nperseg, noverlap=nperseg//2)



    # Limita la frecuencia a 300 Hz para la visualización
    idx = f <= MAX_FREQUENCY

    ### RETORNAR UNA LISTA O ARREGLO NUMPY? 

    data = []

    for j, time in enumerate(t):
        for i, freq in enumerate(f[idx]):
            amplitude = Sxx[i, j]

            temp_time = start_time + time

            amplitude_db = 10*np.log10(Sxx[i,j])

            if amplitude_db > MIN_AMPLITUDE and freq <= MAX_FREQUENCY and freq >= 0.4 :
                data.append([temp_time, freq, amplitude_db, station_name])

    df = pd.DataFrame(data, columns=["datetime_utc", "frequency", "amplitud_db", "station"])
    return df

