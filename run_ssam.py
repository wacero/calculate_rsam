import os
import sys
import logging
import logging.config
from datetime import timedelta, datetime

import pandas as pd
from obspy.core import UTCDateTime

from get_mseed_data import get_mseed
from get_mseed_data import get_mseed_utils as gmutils
from rsam import rsam_utils, db_connection, rsam
from ssam import ssam


if gmutils.check_file("./config/logging.ini"):
    logging.config.fileConfig('./config/logging.ini', disable_existing_loggers=False)
    logger = logging.getLogger('run_rsam')

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('expand_frame_repr', False)


def main():
    """
    ##COMO CORREGIR PROBLEMAS CON ESTACIONES CON GAPS/OVERLAPS? e.g. BRRN
    This script parses the configuration text file, sets the parameters, and runs the rsam module.
    """
    is_error = False

    if len(sys.argv) == 1:
        is_error = True
    else:
        try:
            run_param = gmutils.read_parameters(sys.argv[1])
        except Exception as e:
            logger.error(f"Error reading configuration sets in file: {e}")
            raise Exception(f"Error reading configuration file: {e}")

        try:
            run_mode = run_param['RUN_MODE']['mode']  # Available options are REAL_TIME|FILL
            data_target = run_param['RUN_MODE']['data_target']  # Available options are STDOUT|CSV|DB MSSQL, INFLUX
            db_id = run_param['DATABASE']['id']
            db_config_file = run_param['DATABASE']['file_path']
            db_type = run_param['DATABASE']['type']  # available options are MSSQL|INFLUX
            db_mode = run_param['DATABASE']['mode']  # Available options are INSERT| UPDATE
            mseed_id = run_param['MSEED_SERVER']['id']
            mseed_server_config_file = run_param['MSEED_SERVER']['file_path']
            station_file = run_param['STATION_FILE']['file_path']
            filter_file = run_param['FILTER_FILE']['file_path']
        except Exception as e:
            logger.error(f"Error getting parameters: {e}")
            raise Exception(f"Error getting parameters: {e}")

        try:
            db_param = gmutils.read_config_file(db_config_file)
            mseed_server_param = gmutils.read_config_file(mseed_server_config_file)
            stations_dict = gmutils.read_config_file(station_file)
        except Exception as e:
            logger.error(f"Error reading configuration file: {e}")
            raise Exception(f"Error reading configuration file: {e}")

        try:
            mseed_client = get_mseed.choose_service(mseed_server_param[mseed_id])
        except Exception as e:
            logger.error(f"Failed to create mseed client: {e}")
            raise Exception(f"Failed to create mseed client: {e}")

        """Create filter list"""
        try:
            filter_dict = gmutils.read_config_file(filter_file)
            filter_list = rsam.create_rsamfilter_list(filter_dict)
        except Exception as e:
            logger.error(f"Failed to read filters file: {e}")
            raise Exception(f"Failed to read filters file: {e}")

        """Create streams_list according to run_mode"""
        if run_mode == "REAL_TIME":
            try:
                day_utc = UTCDateTime(UTCDateTime.now().strftime("%Y-%m-%d %H:%M:00")) - 300
                streams_list = rsam.create_stream_list(mseed_id, mseed_client, stations_dict, day_utc, 60, 60, run_mode)
            except Exception as e:
                raise Exception(f"Error getting list of streams: {e}")
        elif run_mode == "FILL":
            if len(sys.argv) == 4:
                start_date = UTCDateTime(datetime.strptime(sys.argv[2], "%Y-%m-%d"))
                end_date = UTCDateTime(datetime.strptime(sys.argv[3], "%Y-%m-%d"))
                streams_list = []
                for day in rsam_utils.perdelta(start_date, end_date, timedelta(days=1)):
                    day_utc = UTCDateTime(day)
                    streams_list.extend(rsam.create_stream_list(mseed_id, mseed_client, stations_dict, day_utc, 86400, 60, run_mode))
            else:
                usage_message = f'USAGE: python {sys.argv[0]} CONFIGURATION_FILE.txt start_date end_date'
                error_message = f"Error in run_mode configured: {run_mode}. start_date and end_date needed"
                print(usage_message)
                logger.error(error_message)
                raise Exception(error_message)
        else:
            logger.error(f"Error in run_mode configured: {run_mode}")
            raise Exception(f"Error in run_mode configured: {run_mode}")


    
        """Create the ssam dataframe using streams_list"""
        try:
            ssam_list = []
            for stream in streams_list[:]:
                ssam_df = ssam. calculate_ssam(stream[0],20, 15,30,60, 0.1250)
                ssam_df['datetime_utc'] = ssam_df['datetime_utc'].apply(lambda x: x.datetime) 
                ssam_df.set_index('datetime_utc',inplace=True)
                ssam_df.index = pd.to_datetime(ssam_df.index)
                ssam_list.append(ssam_df)
                print(ssam_df)
        except Exception as e:
            logger.error(f"Failed to create streams_list: {e}")
            raise Exception(f"Failed to create streams_list: {e}")



        #sys.exit(0)

        """Manage data according to data_target"""
        if data_target == "CSV":
            rsam_dataframe = pd.DataFrame.from_records(ssam_list)
            rsam_dataframe.to_csv(f'./rsam_{start_date}_{end_date}.csv', sep=',', index=False)
        elif data_target == "STDOUT":
            rsam_dataframe = pd.DataFrame.from_records(ssam_list)
            print(rsam_dataframe)

        elif data_target == "DB" and db_type == "INFLUX":
            logger.info("Trying to create influx DF client and influx client")
            
            '''
            influx_df_client = db_connection.get_influx_DF_client(db_param[db_id]['host'],db_param[db_id]['port'],
                                                                db_param[db_id]['user'],db_param[db_id]['pass'],
                                                                db_param[db_id]['DBName'])
            '''

            influx_df_client = db_connection.get_influx_DF_client("192.168.1.90", "8086" , "sangay","sangay2020" ,"ssam")
            
            
            print(influx_df_client)

            for ssam_dataframe in ssam_list:
                db_connection.insert_ssam(ssam_dataframe,influx_df_client)



        else:
            logger.error(f"Error in data_target configured: {data_target}")
            raise Exception(f"Error in data_target configured: {data_target}")

    if is_error:
        logger.info(f'Usage: python {sys.argv[0]} configuration_file.txt [start_date] [end_date]')
        print(f'USAGE: python {sys.argv[0]} CONFIGURATION_FILE.txt [start_date] [end_date]')


if __name__ == "__main__":
    main()
