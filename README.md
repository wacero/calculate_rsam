Code to calculate the RSAM (Real-time Seismic Amplitude Measurement) of seismic waveforms to use as input for an volcanic alert system

Please visit the documentation in the following URL:

https://wacero.gitlab.io/calculate_rsam/

# Create the conda environment
``` bash
$ conda create -n calculate_rsam python=3.7 

$ conda activate calculate_rsam
$ conda install obspy 
$ conda install pyodbc
$ conda install influxdb 
$ conda install pygelf
$ conda install freetds 
$ conda install pandas
$ pip install get_mseed_data
```

# Clone the source code
```
$ cd ~ 
$ git clone https://gitlab.com/wacero/calculate_rsam.git 
```

# Execution

``` bash
$ cd calculate_rsam
$ python ./run_rsam.py CONFIGURATION_FILE.TXT
```


# Connect to a MSSQL DB using Conda and Python

The configuration could be tricky 

``` bash
$ touch $HOME/.odbcinst.ini
##content
[FreeTDS] 
Description=v0.91 with protocol v8.0 
Driver=/home/seiscomp/anaconda3/envs/calculate_rsam/lib/libtdsodbc.so 
UsageCount=1 

# touch /etc/odbcinst.ini 

```

Check the connection to a MSSQL DB
``` bash
$ conda activate calculate_rsam 
$ tsql -H HOST_NAME| HOST_IP -p 58006 -U USUARIO -P CLAVE 
```

# Configuration options

``` bash
['RUN_MODE']
['mode'] 
Available options are REAL_TIME|FILL

['RUN_MODE']
['data_target'] 
Available options are STDOUT|CSV|DB 

['DATABASE']
['type'] 
Available options are MSSQL|INFLUX

['DATABASE']
['mode'] 
Available options are INSERT| UPDATE

```
