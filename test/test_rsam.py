
from rsam import rsam
from obspy import read

data_file="./test/EC.APED..HNZ.2016.107_1908.mseed"
rsam_calculated=1077222.1046

filter_test_file = "./test/EXAMPLE_filter.json"
filter_dict = { "filter_1":{ "name":"rms", "type":"None", "frequency_1":"None", "frequency_2":"None", "order":"None" },
               "filter_2":{  "name":"rsam_banda1",     "type":"bandpass",     "frequency_1":"0.05",     "frequency_2":"0.125" }
               }

def test_rsam_calculate_rsam():
    'Test the calculate_rms method'
 
    data=read(data_file)
    assert round(rsam.calculate_rms(data[0].data),4)==rsam_calculated
    


'''
def test_create_rsamfilter_list():
    
    'Test the create_rsamfilter_list method'
    
    filter_test = rsam.
''' 
